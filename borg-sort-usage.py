import json
import sys

class Archive:
    def __init__(self, name, hash_id, deduplicated_size, original_size):
        self.name = name
        self.hash_id = hash_id
        self.deduplicated_size = deduplicated_size
        self.original_size = original_size
        self.deduplicated_size_gb = deduplicated_size / (1024**3)
        self.original_size_gb = original_size / (1024**3)


    def __str__(self):
        return 'Archive({} - {} - {:5.2f} - {:5.2f})'.format(self.name, self.hash_id, self.deduplicated_size_gb, self.original_size_gb)


print('Number of arguments:', len(sys.argv), 'arguments.')
print('Argument List:', str(sys.argv))
with open(sys.argv[1]) as f:
  data = json.load(f)

all_archives = []

json_archives = data[0]
json_archives = json_archives['archives']

for archive in json_archives:
    stats = archive["stats"]
    deduplicated_size = stats['deduplicated_size']
    original_size = stats['original_size']
    all_archives.append(Archive(archive['name'], archive['id'], deduplicated_size, original_size))

all_archives.sort(key=lambda x: x.deduplicated_size_gb, reverse=True)

sum = 0
print("== All archives by deduplicated size ==\n")
for archive in all_archives:
    sum += archive.deduplicated_size_gb
    print(archive)

print("Total %s:" %sum)
sum = 0
all_archives.sort(key=lambda x: x.original_size_gb, reverse=True)
print("== All archives by original size ==\n")
for archive in all_archives:
    sum += archive.original_size_gb
    print(archive)
print("Original data usage: {:5.2f}TB".format(sum/(1024^3)))